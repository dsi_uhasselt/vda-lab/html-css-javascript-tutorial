const s2 = function(sketch) {
    sketch.setup = function() {
        let canvas = sketch.createCanvas(600,300)
        canvas.parent("movingmouse")
    };

    sketch.draw = function() {
        if ( sketch.mouseIsPressed ) {
            sketch.fill(0);
        } else {
            sketch.fill(255);
        }
        sketch.ellipse(sketch.mouseX, sketch.mouseY, 20, 20)
    }
};
let my_mousemove = new p5(s2);

// function setup() {
//     createCanvas(600,300)
// };

// function draw() {
//     if ( mouseIsPressed ) {
//         fill(0);
//     } else {
//         fill(255);
//     }
//     ellipse(mouseX, mouseY, 20, 20)
// }